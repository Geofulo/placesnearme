//
//  DataManager.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

enum Result<Model> {
    case success(Model)
    case failure(Error)
}

public class DataManager
{
    public let urlString = "https://api.foursquare.com/v2/"
    public let location = "venues/"
    public let clientID = "XXXXXXXX"
    public let clientSecret = "XXXXXXXX"
    public let version = "20190131"
    
    func GetPlaces(latitude:String, longitude:String, offset:Int, completion: @escaping ([ItemModel]?) -> ()) {
        let task = URLSession.shared.dataTask(with: URL(string: "\(urlString)\(location)explore?client_id=\(clientID)&client_secret=\(clientSecret)&v=\(version)&limit=20&offset=\(offset)&ll=\(latitude),\(longitude)")!, completionHandler: { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print("Response Error: \(String(describing: error?.localizedDescription))")
                return
            }
            do {
                let decoder = JSONDecoder()
                let res = try decoder.decode(FSListResponseModel.self, from: dataResponse)
                
                let items = res.response.groups[0].items
                
                print("Response JSON: \(items))")
                
                completion(items)
                return
            } catch let parsingError {
                print("Error", parsingError)
                completion(nil)
                return
            }
        })
        
        task.resume()
    }
    
    func GetPlace(id:String, completion: @escaping (ItemModel?) -> ()) {
        let task = URLSession.shared.dataTask(with: URL(string: "\(urlString)\(location)\(id)?client_id=\(clientID)&client_secret=\(clientSecret)&v=\(version)")!, completionHandler: { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print("Response Error: \(String(describing: error?.localizedDescription))")
                return
            }
            do {
                let decoder = JSONDecoder()
                let res = try decoder.decode(FSSingleResponseModel.self, from: dataResponse)
                
                let model = res.response
                
                print("Response JSON: \(model))")
                
                completion(model)
                return
            } catch let parsingError {
                print("Error", parsingError)
                completion(nil)
                return
            }
        })
        
        task.resume()
    }
    
}
