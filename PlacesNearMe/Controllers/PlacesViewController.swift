//
//  ViewController.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import UIKit
import CoreLocation

class PlacesViewController: UITableViewController {

    @IBOutlet var placesTableView: UITableView!
    
    var places : [ItemModel] = []
    var indexOffset = 0;
    
    let locationManager = CLLocationManager()
    var userLocation : CLLocation!
    
    let dataManager = DataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placesTableView.dataSource = self
//        placesTableView.prefetchDataSource = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func loadPlaces() {
        dataManager.GetPlaces(latitude: String(userLocation.coordinate.latitude), longitude: String(userLocation.coordinate.longitude), offset: indexOffset) { (data) in
            if let data = data {
                self.places.append(contentsOf: data)
                self.indexOffset += 20
                
                DispatchQueue.main.async() {
                    self.placesTableView.reloadData()
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceViewCell") as! PlaceViewCell
        let place = places[indexPath.row]
        
        cell.nameLabel.text = place.venue.name
        cell.addressLabel.text = place.venue.location.formattedAddress?[0]
        
        if let rating = place.venue.rating {
            cell.ratingLabel.text = "\(rating)"
        }
        if let price = place.venue.price {
            cell.priceLabel.text = "\(price)"
        }
        if let distance = place.venue.location.distance {
            cell.distanceLabel.text = "\(distance) km"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.places.count - 8) {
            self.loadPlaces()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailPlaceSegue" {
            let placeVC = segue.destination as? DetailPlaceViewController
            if let index = (placesTableView.indexPathForSelectedRow?.row) {
                placeVC?.placeID = places[index].venue.id
                placeVC?.dataManager = self.dataManager
            }
        }
    }
}

//extension PlacesViewController: UITableViewDataSourcePrefetching {
//    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
//
//        print("Prefetch... \(indexPaths)")
//
//        if indexPaths.contains(where: isLoadingCell) {
//            print("loading more...")
//
//            self.loadPlaces()
//        }
//    }
//}

extension PlacesViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if userLocation == nil {
            userLocation = locations[0]
            self.loadPlaces()
            locationManager.stopUpdatingLocation()
        }
        else {
            userLocation = locations[0]
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
