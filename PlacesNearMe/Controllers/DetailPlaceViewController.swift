//
//  DetailPlaceViewController.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-02-01.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DetailPlaceViewController: UIViewController {
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var placeID : String = ""
    var dataManager : DataManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.showsUserLocation = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        dataManager?.GetPlace(id: placeID) { (place) in
            DispatchQueue.main.async() {
                self.nameLabel.text = place?.venue.name
                self.descriptionLabel.text = place?.venue.description
                self.addressLabel.text = place?.venue.location.formattedAddress?[0]
                self.categoryLabel.text = place?.venue.categories?[0].name
                if let rating = place?.venue.rating {
                    self.ratingLabel.text = "\(rating)"
                    self.ratingLabel.addSubview(blurEffectView)
                }
            }
            do {
                let url = URL(string: (place?.venue.bestPhoto?.GetUrl())!)
                let data = try Data(contentsOf: url!)
                DispatchQueue.main.async() {
                    self.photoImage.image = UIImage(data: data)
                }
            } catch {
                print("No image data")
            }
            
            let lat = place?.venue.location.lat
            let lng = place?.venue.location.lng
            let coordinates = CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
            let region = MKCoordinateRegion(center: coordinates, latitudinalMeters: 500, longitudinalMeters: 500)
            let annotation = MKPointAnnotation.init()
            annotation.coordinate = coordinates
            annotation.title = place?.venue.name
            
            DispatchQueue.main.async() {
                self.mapView.setRegion(region, animated: true)
                self.mapView.addAnnotation(annotation)
            }
        }
    }
}
