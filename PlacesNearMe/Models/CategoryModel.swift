//
//  CategoryModel.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

struct CategoryModel: Codable {
    var id: String
    var name: String
    var primary: Bool?    
}
