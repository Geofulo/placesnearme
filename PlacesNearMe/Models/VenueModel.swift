//
//  VenueModel.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

struct VenueModel: Codable {
    var id: String
    var name: String?
    var description: String?
    var location: LocationModel
    var url: String?
    var rating: Double?
    var categories: [CategoryModel]?
    var price: PriceModel?
    var bestPhoto: PhotoModel?    
}
