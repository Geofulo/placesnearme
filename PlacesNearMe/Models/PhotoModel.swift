//
//  PhotoModel.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

struct PhotoModel: Codable {
    var id: String?
    var prefix : String
    var suffix : String
    var width : Int?
    var height : Int?
    
    func GetUrl() -> String {        
        return "\(prefix)original\(suffix)"
    }
}
