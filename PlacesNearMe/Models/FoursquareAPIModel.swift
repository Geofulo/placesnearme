//
//  ResponseModel.swift
//  PlacesNearMe
//
//  Created by Geovanni on 2/5/19.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

struct FSSingleResponseModel: Codable {
    var response: ItemModel
}

struct FSListResponseModel: Codable {
    var response: ListResponseModel
}

struct ListResponseModel: Codable {
    var groups: [GroupModel]
}

struct GroupModel: Codable {
    var items: [ItemModel]
}
