//
//  PriceModel.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

struct PriceModel: Codable {
    var tier: Int?
    var message: String?
    var currency: String?    
}
