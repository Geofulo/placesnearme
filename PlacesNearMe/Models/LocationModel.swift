//
//  PlaceModel.swift
//  PlacesNearMe
//
//  Created by Geovanni Fuentes on 2019-01-31.
//  Copyright © 2019 Geovanni Fuentes. All rights reserved.
//

import Foundation

struct LocationModel: Codable {
    var street: String?
    var crossStreet: String?
    var lat: Double?
    var lng: Double?
    var distance: Int?
    var postalCode: String?
    var city: String?
    var state: String?
    var country: String?
    var cc: String?
    var formattedAddress: [String]?    
}
